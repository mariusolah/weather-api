import React from 'react'
import PropTypes from 'prop-types'
import format from 'date-fns/format'

const Day = ({ day, list }) => {
	
	return (
		<div>
			{format(new Date(day), 'eeee, dd LLLL')}
			{list.map((item) => {
				const time = item?.dt_txt.slice(10,16);
				return <div key={item?.dt}>{time} | {item?.main.temp} C</div>
			})}
		</div>
	)
}

Day.propTypes = {
	day: PropTypes.string,
	list: PropTypes.array,
}

export default Day
